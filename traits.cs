using System;
using System.Collections;
using System.Collections.Generic;

namespace Trait_tool
{
    /// <summary>
    /// 特性的方法模板
    /// </summary>
    /// <param name="target">特性的接受方</param>
    /// <param name="from">特性的发起方</param>
    /// <param name="public_data">特性的公共数据</param>
    /// <param name="session">发起的会话</param>
    /// <returns></returns>
    public delegate UInt16 method(Hashtable target, Hashtable from, Hashtable public_data, SessionTrait session);

    /// <summary>
    /// 用以初始化特性客户端参数的函数
    /// </summary>
    /// <param name="target"></param>
    public delegate void traits_init(Hashtable target);

    /// <summary>
    /// 完成动作以后对目标的后处理
    /// </summary>
    /// <param name="target">需要后处理的目标</param>
    public delegate void traits_update(object target, Hashtable target_data);

    /// <summary>
    /// Traits 特性 的抽象表达
    /// 用以管理每个特性功能能的处理向量
    /// </summary>
    public class Traits : Hashtable
    {
        /// <summary>
        /// 特性的名称
        /// </summary>
        public string name;
        /// <summary>
        /// 特性的哈希 用以一对多调用
        /// 当特性的哈希值为 0x0000 接受一切同名调用
        /// 当查询的哈希值为 0x9999 调用一切同名特性
        /// </summary>
        public UInt16 hash; 

        /// <summary>
        /// 处理特性内共享的全局数值
        /// </summary>
        public Hashtable data;

        /// <summary>
        /// 初始化客户端的构造函数
        /// </summary>
        public traits_init Init;

        /// <summary>
        /// 更新后处理
        /// </summary>
        public traits_update Update;
        
        
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="n">特性名</param>
        /// <param name="h">特性哈希</param>
        public Traits(string n, traits_init i, UInt16 h = 0x0000){
            Init = i;
            name = n.Trim();
            hash = h;
            data = new Hashtable();
        }
        
        /// <summary>
        /// 增加一个特性的方法
        /// </summary>
        /// <param name="n">方法名</param>
        /// <param name="f">方法的处理函数</param>
        public void Add(string n, method f){
            base.Add(n, f);
        }

        /// <summary>
        /// 判断特性是否相等
        /// </summary>
        /// <param name="n">方法名称</param>
        /// <param name="h">目标哈希</param>
        /// <returns>返回是否相等</returns>
        public bool equal(string n, UInt16 h){
            if(n != name) return false;
            if(h == 0x9999 || hash == 0x0000) return true;
            return h == hash;
        }
    }

    /// <summary>
    /// 一个呼叫Trait的会话
    /// </summary>
    public class SessionTrait
    {

        struct Step{
            public Guid trait_name;
            public string method_name;
        };
        public bool finish;

        ClientTrait from_obj;
        ClientTrait target_obj;
        object base_object;
        public DelegateTrait delegates;
        public Hashtable data;
        List<Step> steps;

        /// <summary>
        /// 执行完毕后的返回值
        /// </summary>
        public UInt16 return_data;

        public SessionTrait(){  
            data = new Hashtable();
            finish = false;
            steps = new List<Step>();
        }

        /// <summary>
        /// 初始化一个会话
        /// </summary>
        /// <param name="f">会话发起方特性实例</param>
        /// <param name="t">会话目标方特性实例</param>
        /// <param name="d">会话查询环境</param>
        /// <param name="bo">目标基本实例</param>
        public void Init(ClientTrait f, ClientTrait t, DelegateTrait d, object bo){
            from_obj = f; target_obj = t;
            delegates = d;base_object=bo;
        }

        /// <summary>
        /// 增加一个会话操作
        /// </summary>
        /// <param name="tn">会话步骤特性名</param>
        /// <param name="mn">会话步骤方法名</param>
        public void Add(Guid tn, string mn) {
            Step step = new Step();

            step.trait_name = tn;
            step.method_name = mn;
            steps.Add(step);
        }
        public void Add(string tn, string mn, UInt16 hash = 0x0000){
            foreach(Guid i in delegates.Search(tn, hash)){
                Add(i, mn);
            }
            
        }


        /// <summary>
        /// 执行会话步
        /// </summary>
        /// <returns>待执行列表为空则返回false</returns>
        public bool Run(){
            foreach(Step step in steps){
                method foo = (method)((Traits)delegates.traits[step.trait_name])[step.method_name];
                return_data = foo(target_obj.traits, from_obj.extra_data, (Hashtable)((Traits)delegates.traits[step.trait_name]).data, this);
            }
            return true;
        }
    
        /// <summary>
        /// 执行更新， 将特性实例数据反映到实例本身
        /// </summary>
        /// <param name="b"></param>
        public void Update(){
            foreach(Step step in steps){
                ((Traits)delegates.traits[step.trait_name]).Update(base_object, target_obj.traits);
            }
        }

        public SessionTrait Clone(){
            return (SessionTrait)this.MemberwiseClone();
        }
    }

    public class ClientTrait
    {
        /// <summary>
        /// 参数表
        /// </summary>
        public Hashtable traits;
        
        /// <summary>
        /// 绑定的服务端
        /// </summary>
        DelegateTrait delegates;
        
        /// <summary>
        /// 作为执行发起方提供的参数
        /// </summary>
        public Hashtable extra_data;

        public ClientTrait(DelegateTrait d){
            delegates = d;
            traits = new Hashtable();
            traits.Add("this", this);
        }
        /// <summary>
        /// 向参数表增加一个参数
        /// </summary>
        /// <param name="n">参数名</param>
        public void Add(string n){
            Hashtable paras = new Hashtable();
            paras.Add("changed", false);
            traits.Add(n, paras);
            delegates.Init(traits, n);
        }

        /// <summary>
        /// 被作为目标引用时执行
        /// </summary>
        /// <param name="t">所指定的特性</param>
        /// <param name="n">特性的方法</param>
        /// <param name="f">指定的来源</param>
        /// <param name="b">执行源</param>
        /// <param name="s">执行会话</param>
        /// <returns>执行结果</returns>
        public UInt16 RTCall(string t, string n, ClientTrait f, object b, SessionTrait s){
            s.Init(f, this, delegates, b);
            s.Add(t, n);
            s.Run();
            s.Update();
            return s.return_data;
        }
    }



    /// <summary>
    /// 服务端集中管理特性的耦合体
    /// </summary>
    public class DelegateTrait 
    {
        /// <summary>
        /// 储存和管理特性列表
        /// </summary>
        public Hashtable traits;
        /// <summary>
        /// 储存和管理会话
        /// </summary>
        public List<SessionTrait> sessions;
        public Queue runnings;

        Hashtable search_table;

        public DelegateTrait(){
            traits = new Hashtable();
            sessions = new List<SessionTrait>();
            runnings = new Queue();
            search_table = new Hashtable();
        }
        public Guid Add(String name, traits_init i, UInt16 hash=0x0000){
            Traits t = new Traits(name, i, hash);
            Guid g = Guid.NewGuid();
            traits.Add(g, t);

            if(search_table.ContainsKey(name)) ((List<Guid>)search_table[name]).Add(g);
            else{
                search_table[name] = new List<Guid>();
                ((List<Guid>)search_table[name]).Add(g);
            }
            return g;
        }
        public SessionTrait Session(){
            SessionTrait t = new SessionTrait();
            sessions.Add(t);
            return t;
        }
        public void UpdateSlow(){
            for(int i = sessions.Count-1; i>=0; i--){
                if(sessions[i].finish){
                    sessions[i].Run();
                    runnings.Enqueue(sessions[i]);
                    sessions.RemoveAt(i);
                }
            }
        }
        public void UpdatePerFrame(){
            while(runnings.Count != 0){
                SessionTrait t = (SessionTrait)runnings.Dequeue();
                t.Update();
            }
        }
        public List<Guid> Search(string n, UInt16 h){
            if(search_table.ContainsKey(n)){
                List<Guid> l = new List<Guid>();
                foreach(Guid i in ((List<Guid>)search_table[n])){
                    if(((Traits)traits[i]).equal(n, h)){
                        l.Add(i);
                    }
                }
                return l;
            }
            return new List<Guid>();
        }

        /// <summary>
        /// 初始化一个特性储存结构
        /// </summary>
        /// <param name="t">储存结构的目标对象</param>
        /// <param name="n">储存对象的Guid</param>
        public void Init(Hashtable t, Guid n) {
            ((Traits)traits[n]).Init(t);
        }
        /// <summary>
        /// 初始化一个特性储存结构
        /// </summary>
        /// <param name="t">储存结构的目标对象</param>
        /// <param name="n">储存对象的名称</param>
        /// <param name="h">储存对象的哈希</param>
        public void Init(Hashtable t, string n, UInt16 h = 0x9999) {
            foreach(Guid i in Search(n, h)){
                ((Traits)traits[i]).Init(t);
            }
        }
    }
}
