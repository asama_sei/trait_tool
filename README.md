### DelegrateTrait 特性服务端

DelegrateTrait 是一个服务端
提供了一个语义环境下的所有共有特性项目的管理
并提供了基本的控制流程

### 新建服务端

可以在任何情况下新建一个特性服务端用以创造一个纯净的语义环境

### 增加服务端特性

通过以下调用可以在一个服务端环境中新建一个特性
 *DelegrateTrait#Add*
(string Name, traits_init foo, UInt16 h)

* Name为一字符串，用以标注特性名称
  * Name不唯一，可能有重复
  * 同名特性通过哈希来进行使用时区分
  * 定义时哈希**0x0000**会接受任意同名调用
  * 使用时哈希**0x9999**会调用全部同名函数
* traits_init 为一个初始化函数
  * 格式为*void* traits_init(*Hashtable* target)
  * target 为客户端维护的数据表
  * 此函数是用来在绑定开始阶段，在客户端的数据表中创造初始数据
  * 可以通过*target["this"]*访问源头*ClientTrait*
* 函数会返回一Guid类型对象
  * 该对象全局唯一
  * 用以在构建过程中保证目标对象的可达性所提供的耦合接口



一个典型的新建并获取刚刚建立完成的对象可以是如下这样：

```c#
Guid guid = delegate_trait.Add("Name", delegate(Hashtable ht){ .. }, 0x0020);
Traits trait = (Traits)delegate_trait.traits[guid]
```

最终trait为一个类型为Traits 储存于 delegate_trait.traits 哈希表内 的实际对象引用

### 完善一个特性

当新建了一个特性以后，需要如下方法进行完善

* 完成 Update 方法
  * 格式为 *void* traits_update(*object* target, *Hashtable* data)
  * 在逻辑式运行过程中，Data为触发式修改，这个修改是数据层面的
  * 每隔固定时间（比如一帧）会进行一次update，在update时，会调用这个函数。因此，这个函数应可以使data的内容体现到一个最终层面
  * 例如：某个程序在data中修改了一个立方体的边长，这个边长不会立刻体现到实际的立方体对象上，而是暂存在data表中。而这个函数的工作，就是每隔一段时间，获取data表的改动并将其应用在target上。
  * 对于任意一个对象，此处的data表与前文中traits_init(*Hashtable* target)的target表为同一数据表
* 添加方法对
  * 使用 *Traits#Add(string name, method f)*来添加一个方法对
  * name为方法名称，应在特性内唯一
  * method 格式为： UInt16 method(Hashtable target, Hashtable from, Hashtable public_data, SessionTrait session);
  * target 是作为这个方法接收方数据表
  * from 为一个专用的发起方字段，其位置在 ClientTrait#extra_data
  * public_data 为特性固有的全局参数（例如记录初始化次数）
  * session 为提供了一个执行时的环境 可以通过*SessionTrait#data*交换信息

### 创建一个新客户

在我们拥有了一个服务端，并且在服务端中加入了特性以后，我们可以在这个服务端环境下建立客户端
使用 *ClientTrait#New(DelegrateTrait dele)* 将在dele服务端环境下新建一个客户端

对新生成的实例调用 *ClientTrait#Add(string n)* 可以为这个客户加入特性
此时，会自动在traits建立同名哈希，并调用特性对应的Init方法对参数表进行初始化

### 执行一次调用

* 首先使用 *DelegrateTrait#Session()* 新建一个服务端维护的会话，或直接new一个独立会话
* 调用 *SessionTrait#Init(ClientTrait f, ClientTrait t, DelegateTrait d, object bo)* 完成环境初始化
  * f为调用发起方
  * t为调用作用方
  * d为服务端
  * bo为最终实现效果的目标对象封装
* 调用 *SessionTrait#Add* 来填充步骤，可以使用名称和哈希来指定多个执行目标，或者使用Guid选择唯一执行目标
* 服务端维护的会话，当 *SessionTrait#finish* 为真时，会自动调用Run与Update执行和更新

### 其他
* *ClientTrait#RTCall*正在完善中，是一个即使调用及时反馈并作用在对象上的捷径
* 目前hash大于0xA000的特新为预留区域
